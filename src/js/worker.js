if("serviceWorker" in navigator){
  navigator.serviceWorker.register(".sw.js").then((reg) => {
    console.log("Service Worker Registered", reg);
  }).catch((err) => {
    console.log("Service Worker Not Registered", err);
  });
}

// Push Notification
function askForNotificationPermission() {
  Notification.requestPermission().then((permission) => {
    console.log(permission);
    if (permission === "granted") {
      displayNotification();
    } else {
      console.log("Permission not granted to show notifications");
    }
  });
}

function displayNotification() {
  if (Notification.permission === "granted") {
    navigator.serviceWorker.getRegistration().then((reg) => {
      const options = {
        body: "This notification was generated from a push!",
        icon: "./img/logo192.png",
        vibrate: [100, 50, 100],
        data: {
          dateOfArrival: Date.now(),
          primaryKey: 1,
        },
        actions: [
          {
            action: "explore",
            title: "Go to the site",
            icon: "./img/logo192.png",
          },
          { action: "close", title: "Close the notification", icon: "./img/logo192.png" },
        ],
      };
      reg.showNotification("Hello world!", options);
    });
  }
}

//askForNotificationPermission();
