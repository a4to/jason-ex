const cacheName = 'v1';
const cacheAssets = [
  './',
  './index.html',
  './src',
  './src/js/worker.js',
  './src/css/global.css',
  './src/img/adaptive-icon.png',
  './src/img/apple-touch-icon.png',
  './src/img/favicon.png',
  './src/img/logo512.png',
  './src/img/logo192.png',
  './src/img/splash.png',
];
self.addEventListener('install', e => {
  console.log('Service Worker: Installed');
  e.waitUntil(
    caches
      .open(cacheName)
      .then(cache => {
        console.log('Service Worker: Caching Files');
        cache.addAll(cacheAssets);
      })
      .then(() => self.skipWaiting())
  );
});
self.addEventListener('activate', e => {
  console.log('Service Worker: Activated');
  e.waitUntil(
    caches.keys().then(cacheNames => {
      return Promise.all(
        cacheNames.map(cache => {
          if (cache !== cacheName) {
            console.log('Service Worker: Clearing Old Cache');
            return caches.delete(cache);
          }
        })
      );
    })
  );
});
self.addEventListener('fetch', e => {
  console.log('Service Worker: Fetching');
  e.respondWith(fetch(e.request).catch(() => caches.match(e.request)));
});
self.addEventListener('push', e => {
  const data = e.data.json();
  console.log('Push Received...');
  self.registration.showNotification(data.title, {
    body: 'Notified by PWA',
    icon: '/img/logo192.png'
  });
});
